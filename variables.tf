variable "aci_url" {
  type = string
}

variable "aci_username" {
  type = string
}

variable "private_key_name" {
  default = "terraform"
  type = string
}
variable "private_key_data" {
  type = string
}